package com.daniilk.intensive1

import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Устанавливаем дату
        val sdf = SimpleDateFormat("dd.MM.yyyy hh:mm:ss")
        val currentDate = sdf.format(Date())
        findViewById<TextView>(R.id.txtDate).setText(currentDate+"");

        // Красим навигационный бар - где мой Java :(
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.navigationBarColor = resources.getColor(R.color.teal_700)
        }
    }

    fun onData(view: View) {
        // Где мой любимый switch :(
        when (view.id) {
            R.id.btnCancel -> Toast.makeText(this, "Click Button Cancel", Toast.LENGTH_SHORT).show()
            R.id.btnSave -> Toast.makeText(this, "Click Button Save", Toast.LENGTH_SHORT).show()
            R.id.btnShare -> Toast.makeText(this, "Click Button Share", Toast.LENGTH_SHORT).show()
        }
    }
}